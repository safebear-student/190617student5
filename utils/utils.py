from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities

class Parameters():
    def __init__(self):
        # self.w = webdriver.Chrome()
        self.w = webdriver.Remote(command_executor='http://127.0.0.1:4444/wd/hub',desired_capabilities=DesiredCapabilities.HTMLUNITWITHJS)
        self.rootUrl = "http://automate.safebear.co.uk"
        self.w.implicitly_wait(5)
        #self.w.username = testuser
        #self.w.password = testing
